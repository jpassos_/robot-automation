*** Settings ***
Resource  ../base.robot

*** keywords ***

Dado que foi aberto o site do ml
    Open Browser    ${baseUrlMl}  ${browser}
    Click Element   ${page_ml.button_navbar}

Quando digitar ${PESQUISA}
    Input Text   ${page_ml.text_pesquisa}      ${pesquisaProduto}

E clicar no botão pesquisar
    Click Element  ${page_ml.button_pesquisar}
    Take Screenshot  teste.jpg  80%

Então deve encerrar a aplicação
    Close Browser