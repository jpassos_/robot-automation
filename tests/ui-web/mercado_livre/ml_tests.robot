*** Settings ***
Resource  base.robot
Resource  keywords/ml_keywords.robot

Documentation  Testando abrir o site do mercado livre e fazer uma pesquisa

*** Test Cases ***

Cenário 01: Abrindo página do mercado livre

    Dado que foi aberto o site do ml
    Quando digitar ${pesquisaProduto}
    E clicar no botão pesquisar
    Então deve encerrar a aplicação.